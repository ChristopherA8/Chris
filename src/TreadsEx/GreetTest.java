package TreadsEx;

public class GreetTest {

	public static void main(String[] args) {
		
		Runnable greet = new Greet();
		
		Thread td = new Thread(greet);
		td.setName("Greeter Thread");
		
		td.start();

	}

}
