package TreadsEx;

public class Greet implements Runnable {
	
	public void run(){
		Thread t = Thread.currentThread();
		System.out.println(t.getName() + " thread says: Hello!");
	}

}
