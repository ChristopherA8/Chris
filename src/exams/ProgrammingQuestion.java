package exams;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;

public class ProgrammingQuestion {

	public static void main(String[] args) {
		
		String fileName = "input.txt";
		
		boolean bool = true;
		
		BufferedReader br = null;
		ArrayList<String> lines = new ArrayList<String>();
		Iterator<String> iterator = lines.iterator();
		try{
			FileReader fr = new FileReader(fileName);
			br = new BufferedReader(fr);
			String text;
			while(true){
			try {
				text = br.readLine();
				System.out.println(text);
				if(text == null){
					break;
				}
				lines.add(text);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			}
			System.out.println(lines);
			
			for(int i =0; i<101; i++){
				if(i % 2 == 0){
					System.out.println();
				}else{
					String[] line = lines.get(i).split(",");
					for(int j = 0; j < 4; j++){
						System.out.print(line[j] + "\t" + "\t");
					}
					System.out.println("");
				}
			}
			
			/*while(bool){
				try {
					String line = br.readLine();
					if(line == null){
						break;
					}
					String[] splitLine = line.split(",");
					if (splitLine.length > 0){
						for(int i = 0; i < 4; i++){
							System.out.print(splitLine[i] + "\t" + "\t");
						}
						System.out.println("");
					}else{
						br.skip(1);	
					}
					
					
				}catch(IOException e){
					e.printStackTrace();
				}
			}*/
		}catch(FileNotFoundException ex){
			ex.printStackTrace();
		}finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		

	}

}
