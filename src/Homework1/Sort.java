package Homework1;

import java.util.Arrays;

public class Sort {
	
	public void bubbleSort(int[] array) {
	      boolean s = true;
	      int temp;
	      while (s) {
	            s = false;
	            for (int i: array) {                                       
	                  if (array[i] > array[i + 1]) {                          
	                        temp = array[i];
	                        array[i] = array[i + 1];
	                        array[i + 1] = temp;
	                        s = true;
	                  }
	            } 
	       System.out.println(Arrays.toString(array));     
	      }
	}

}
