package Homework1;

import java.util.ArrayList;

public class Primes {

	public static void main(String[] args) {
		
		//Set up the ArrayList
		ArrayList<Integer> nums = new ArrayList<Integer>();
		for(int i = 1; i <= 100; i++){
			nums.add(i);
		}
		
		//Remove the composite numbers
		ArrayList<Integer> prime = new ArrayList<Integer>();
		prime = nums;
		ArrayList<Integer> composit = new ArrayList<Integer>();
		for(int j = 0; j < 11; j++){
			for(int k = 2; k < j; k++){
				if(j % k == 0){
					composit.add(nums.get(j - 1));
					break;
				}
			}
		}
		prime.removeAll(composit);
		System.out.println("" + prime);

	}

}
