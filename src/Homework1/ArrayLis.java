package Homework1;

import java.util.*;

public class ArrayLis {

	public static void main(String[] args) {
		
		//Setting up the ArrayList
		ArrayList<Integer> nums = new ArrayList<Integer>();
		for(int i = 1; i <= 10; i++){
			nums.add(i);
		}
		System.out.println("" + nums);
		
		//Adding the even numbers
		int counter = 0;
		for(int n : nums){
			if(n % 2 == 0){
				counter = counter + n;
			}
		}
		System.out.println("" + counter);
		
		//Adding the odd numbers
		int counter2 = 0;
		for(int n : nums){
			if(n % 2 == 1){
				counter2 = counter2 + n;
			}
		}
		System.out.println("" + counter2);
		
		//Removing Primes
		ArrayList<Integer> composit = new ArrayList<Integer>();
		for(int j = 0; j < 11; j++){
			for(int k = 2; k < j; k++){
				if(j % k == 0){
					composit.add(nums.get(j - 1));
					break;
				}
			}
		}
		nums = composit;
		System.out.println("" + nums);
	}

}
