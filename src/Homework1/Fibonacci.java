package Homework1;

import java.util.Arrays;

public class Fibonacci {

	public static void main(String[] args) {
		int[] fib = new int[25];
		fib[0] = 1;
		fib[1] = 1;
		int i = 2;
		while(i < 25){
			fib[i] = fib[i-1] + fib [i -2];
			i++;
		}
		System.out.println(Arrays.toString(fib));

	}

}
