package Homework1;

public class SubCls extends SuperCls {
	
	//Empty Methods
	public void upperCheck(){
		System.out.println("Input String");
	}
	public void convert(){
		System.out.println("Input String");
	}
	public void integer(){
		System.out.println("Input String");
	}
	//Inheritance
	public void upperCheck(String s){
		boolean tf = true;
		String us = s.toLowerCase();
			if(s.equals(us)){
				tf = false;
			}
		System.out.println("Uppercase characters? " + tf);
	}
	public void convert(String st){
		System.out.println(st.toUpperCase());
	}
	public void integer(String str){
		int x = Integer.parseInt(str);
		x += 10;
		System.out.println("" + x);
	}

}
