package Homework1;

import java.io.*;

public class Reader {

	public static void main(String[] args) {
		
		String fileName = "Data.txt";
		
		boolean bool = true;
		
		BufferedReader br = null;
		
		try {
			FileReader fr = new FileReader(fileName);
			br = new BufferedReader(fr);
			while(bool){
				try {
					String line = br.readLine();
					if(line == null){
						break;
					}
					String[] splitLine = line.split(":");
					System.out.println("Name: " + splitLine[0] + " " + splitLine[1] );
					System.out.println("Age: " + splitLine[2] + " years");
					System.out.println("State: " + splitLine[3] + " State");
					System.out.println("");
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		

	}

}
