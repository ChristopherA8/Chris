package Homework1;

import java.util.*;

public class ScannerEx {

	public static void main(String[] args) {
		
		double principal;
		final double rate;
		int time;
		double interest;
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Principal?");
		principal = scan.nextDouble();
		
		
		
		System.out.println("Rate?");
		rate = scan.nextDouble();
		
		
		
		System.out.println("Time?");
		time = scan.nextInt();
		scan.close();
		
		interest = principal * rate * time;
		System.out.println("Interest is: " + interest);

	}

}
