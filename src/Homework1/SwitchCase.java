package Homework1;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class SwitchCase {
	
	void swch(int cas){
		if(!(cas == 1) && !(cas == 2) && !(cas == 3)){
			cas = 4;
		}
		switch(cas){
		case 1: 
			System.out.println("The square root of 25 is : " + Math.sqrt(25));
			break;
		case 2: 
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			Date date = new Date();
			System.out.println(dateFormat.format(date));
			break;
		case 3:
			String str = new String("I am learning Core Java");
			String[] ary = str.split("");
			for(int i = 0; i < ary.length; i++){
				System.out.println(""+ary[i]);
			}
			break;
		case 4:
			System.out.println("Invalid case");
		}
	}

}
