package Homework1;

import java.util.*;
import java.io.*;

public class Palindromes {

	public static void main(String[] args) {
		
		ArrayList<String> words = new ArrayList<String>(Arrays.asList("karan", "madam", "tom", "civic", "radar", "sexes", "jimmy", "kayak", "john", "refer", "billy", "did"));
		ArrayList<String> palins = new ArrayList<String>();
		
		//StringQ is the reverse class
		StringQ modifier = new StringQ();
		
		for(String i : words){
			if(i.equals(modifier.reverse(i))){
				palins.add(i);
			}
		}
		//Checking that the palins ArrayList is set up correctly
		System.out.println("" + words.get(0));
		System.out.println("" + palins.get(0));
		
		//Writing a file
		File fileName = new File("palindromes.txt");
		
		try{
			FileWriter fileWriter = new FileWriter(fileName);
			
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
			
			for(String j: palins){
				bufferedWriter.write(j);
				bufferedWriter.newLine();
			}
			bufferedWriter.close();
		}
		catch(IOException ex){
			System.out.println("Error in writing file.");
		}
		
		//Finding the file
		 System.out.printf("File is located at %s%n", fileName.getAbsolutePath());

	}

}
