package dummypackage;

import java.util.*;
	
public class EmployeeArray {
	

	public static void main(String[] args) {
		
		//ArrayList setup
		
		Employee chris = new Employee();
		Employee omar = new Employee("Omar", "Engineering", 25);
		Employee will = new Employee("Will", "Accounting", 24);
		
		List<Employee> programmers = new ArrayList<Employee>();
		programmers.add(chris);
		programmers.add(will);
		programmers.add(omar);
		
		//Printing using Iterator unsorted
		
		System.out.println("Unsorted");
		
		Iterator<Employee> iterator = programmers.iterator();
		while(iterator.hasNext()){
			Employee element = iterator.next();
			System.out.println(element.getName() + "'s age is " + element.getAge() + ", and their department is " + element.getDepartment());
		}
		
		//Sorting according to age
		Collections.sort(programmers, new AgeCampare());
		System.out.println("");
		System.out.println("Age sort");
		
		Iterator<Employee> itr = programmers.iterator();
		while(itr.hasNext()){
			Employee element = itr.next();
			System.out.println(element.getName() + "'s age is " + element.getAge() + ", and their department is " + element.getDepartment());
		}
		
		//Sort according to name
		Collections.sort(programmers, new NameCompare());
		
		System.out.println("");
		System.out.println("Name sort");
		
		Iterator<Employee> it = programmers.iterator();
		while(it.hasNext()){
			Employee element = it.next();
			System.out.println(element.getName() + "'s age is " + element.getAge() + ", and their department is " + element.getDepartment());
		}
		
		//Sort according to Department
		Collections.sort(programmers, new DeptCompare());
		
		System.out.println("");
		System.out.println("Department sort");
		
		Iterator<Employee> iter = programmers.iterator();
		while(iter.hasNext()){
			Employee element = iter.next();
			System.out.println(element.getName() + "'s age is " + element.getAge() + ", and their department is " + element.getDepartment());
		}

	}

}
