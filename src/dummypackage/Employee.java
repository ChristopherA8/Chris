package dummypackage;

public class Employee {
	
	String name;
	String department;
	int age;
	
	public Employee(){
		name = "Chris";
		department = "Development";
		age = 26;
	}
	
	public Employee(String n, String d, int s){
		name = n;
		department = d;
		age = s;
	}
	
	public String getDepartment(){
		return department;
	}
	
	public String getName(){
		return name;
	}
	
	public int getAge(){
		return age;
	}
	
	
}
