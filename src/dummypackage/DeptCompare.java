package dummypackage;

import java.util.*;

public class DeptCompare implements Comparator<Employee> {
	
	public int compare(Employee a, Employee b){
		return a.department.compareToIgnoreCase(b.department);
	}

}
