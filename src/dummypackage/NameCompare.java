package dummypackage;

import java.util.*;

public class NameCompare implements Comparator<Employee> {
	
	public int compare(Employee a, Employee b){
		return a.name.compareToIgnoreCase(b.name);
	}
	
}
