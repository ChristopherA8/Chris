package dummypackage;

import java.util.*;

public class AgeCampare implements Comparator<Employee> {
	
	public int compare(Employee a, Employee b) {
        return a.age < b.age ? -1 : a.age == b.age ? 0 : 1;
    }

}
